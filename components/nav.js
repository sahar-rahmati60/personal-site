import React from 'react'
import Link from 'next/link'

const links = [
/*   { href: '/blog', label: 'Blog' }, */
  { href: '/resume', label: 'Resume' },
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`
  return link
})

const Nav = () => (
  <nav>
    <ul>
      <li>
        <Link href="/">
          <a>Home</a>
        </Link>
      </li>
      {links.map(({ key, href, label }) => (
        <li key={key}>
          <a href={href}>{label}</a>
        </li>
      ))}
    </ul>

    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
        background-color: #eff2f5
      }
      nav {
        text-align: left;
      }
      ul {
        display: flex;
        margin: 20px 10px 0;
      }
      li {
        display: flex;
        padding: 6px 8px;
      }
      a {
        text-decoration: none;
        font-size: 20px;
        color: #4f5057;
      }
    `}</style>
  </nav>
)

export default Nav

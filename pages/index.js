import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Nav from '../components/nav'

const Home = () => (
  <div>
    <Head>
      <title>Sahar Rahmati</title>
    </Head>
    <Nav />
    <div className="intro-box">
      <div className="main-box">
        <div className="img-box">
          <img className="profilr-pic" src="/sahar.jpg" title="Sahar Rahmati" allt="" />
        </div>
        <div>
          <h1>Sahar Rahmati </h1>
          <p>
          I'm Sahar Rahmati, a Frontend developer based in Tehran. I enjoy programming and learning about new technologies.
          </p>
          <ul>
            <li>
              <Link href="https://twitter.com/sahar_r60">
                <a> <img src="/Twitter.png" title="twitter" allt="" /></a>
              </Link>
            </li>
            <li>
              <Link href="https://linkedin.com/in/sahar-rahmati-2201355a">
                <a> <img src="/LinkedIn.png" title="linkedin" allt="" /> </a>
              </Link>
            </li>
            <li>
              <Link href="https://t.me/Shrmt">
                <a>  <img src="/Telegram.png" title="telegram" allt="" /> </a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <style jsx>{`
      .main-box{
        display: flex;
        align-items: center;
        margin-top: 165px;
      }
      @media (max-width : 574px) {
        .main-box {
          display: block;
          text-align: center;
          margin-top: 80px;
          padding-bottom: 20px;
        }
      }
      .intro-box {
        width: 40%;
        margin: auto;
        color: #333;
        background-color: white;
        box-shadow: 0 3px 60px 0 rgba(79,80,87,.3);
        border-radius: 200px 30px 30px 200px;
        marging-top: 80px;
      }
      @media (max-width : 1400px) {
        .intro-box {
          width: 80%;
        }
      }
      @media (max-width : 767px) {
        .intro-box {
          width: 97%;
          border-radius: 30px;
        }
      }
      .img-box {
        padding: 15px 15px 10px;
      }
      .profilr-pic {
        border-radius: 50%;
        border: 15px solid #f1f1f1;
      }
      @media (max-width : 767px) {
        .profilr-pic {
          width: 150px
        }
      }
      h1 {
        color: #4f5057;
      }
      @media (max-width : 767px) {
        h1{
          font-size: 18px;
        }
      }
      p {
        color: #4f5057;
        padding-right: 66px;
        margin-bottom: 40px;
        color:#4f5057;
        opacity: .5;
      }
      @media (max-width : 767px) {
        p {
          padding-right: 20px;
        }
      }
      ul {
        padding-left: 5px;
      }
      li {
        display: inline-block;
        padding: 0 10px;
      }
    `}</style>
  </div>
)

export default Home

import React from 'react'
import Nav from '../components/nav'

const Rssume = () => (
  <div>
    <Nav />
    <div className="resume-block">
        <div className="tel-email">
            <span className="email">Email: sahar.rahmati60@gmail.com</span>
            <span className="tel">Mobile No: 09123961598 </span>
        </div> 
        <div className="technical-skills">
            <h3>TECHNICAL SKILLS</h3>
            <ul>
                <li>Intermediate knowledge of React and React hooks</li>
                <li>Intermediate knowledge of Vue.js and Vuex</li>
                <li>Intermediate knowledge of ES6</li>
                <li>Solid knowledge of HTML5, CSS and SASS</li>
                <li>Proficient in CSS frameworks (Bootstrap, Material Components)</li>
                <li>Competent in NPM Scripts, Gulp, Grunt</li>
                <li>Extensive experience with Git version control system</li>
                <li>Working knowledge of Linux</li>
                <li>Intermediate knowledge of relational databases (Mysql)</li>
                <li>Intermediate knowledge of PHP</li>
            </ul>
        </div>
        <div className="experienece">
            <h3>EXPERIENCE</h3>
            <h4>Tehranmelody, Tehran — 
                <span className="title">Frontend developer</span> -  
                <span className="date"> Oct 2017 – Nov 2019</span>
            </h4>
            <ul>
                <li>Developed a responsive main template and over 10 internal pages with HTML5, Bootstrap V4, Sass and Gulp</li>
                <li>Developed the responsive Email templates</li>
                <li>Developed 5 responsive landing Pages</li>
                <li>Developed a buying process and user profile section of the website with Vue.js and Vuex</li>
                <li>Developed a leasing request form and its process with React</li>
            </ul>
            <h4>ACA, Tehran — 
                <span className="title">Frontend developer</span> -  
                <span className="date"> Mar 2009 – Jul 2017</span>
            </h4>
            <ul>
                <li>Developed over 40 responsive template of Iran university with HTML5, Bootstrap , Sass , jquery</li>
                <li>Developed and maintained PHP packages for inhouse CMS</li>
            </ul>
        </div>
        <div className="education">
            <h3>EDUCATION</h3>
            Payam e Noor, Tehran — <span>B.S. in  Industrial Engineering</span>
        </div>
        <div className="language">
            <h3>LANGUAGES</h3>
            <ul>
                <li>Farsi : <span>native language</span></li>
                <li>English : <span>Intermediate</span></li>
                <li>Turkish : <span>proficient in spoken</span></li>
            </ul>
        </div>
    </div>
    <style jsx>{`
        .resume-block {
            width: 50%;
            margin: auto;
            color:#333;
            background-color:white;
            box-shadow: 0 3px 60px 0 rgba(79,80,87,.3);
            border-radius: 30px;
            padding: 12px 60px 60px;
            margin-bottom: 100px;
            margin-top: 100px;
        }
        @media (max-width : 1199px) {
            .resume-block {
              width: 70%;
              padding: 12px 30px 60px;
            }
        }
        .tel-email {
            text-align: center;
            border-bottom: 1px solid #95989a;
            padding: 20px;
        }
        .tel-email span {
            color: #95989a;
            padding-right: 20px;
            padding-left: 20px;
        }
        @media (max-width : 800px) {
            .tel-email span {
              display: block;
              padding-bottom: 10px;
              padding-right: 0;
              padding-left: 0
            }
        }
        .technical-skills {
            margin-bottom: 30px;
        }
        li {
            font-size: 14px;
            color: #4f5057;
        }
        h3 {
            color: #95989a;
        }
        h4 {
            margin-bottom: 0;
        }
        .experienece {
            color: #95989a;
            margin-bottom: 30px;
        }
        .experienece .date {
            font-size: 10px;
        }
        .experienece .title {
            font-style: italic;
            font-size: 13px
        }
        .education {
            margin-bottom: 30px;
        }
        .education span {
            font-style: italic;
            color: #95989a;
        }
        .language span {
            font-style: italic;
            color: #95989a; 
        }
    `}</style>
  </div>
)

export default Rssume
